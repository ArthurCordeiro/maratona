#include <bits/stdc++.h>

using namespace std;
int marked[200005], n;
vector<int> s1, s2;
int main()
{
	cin >> n;
	bool flag = true;
	for(int i = 0 ; i < n; ++i)
	{
		int x; cin >> x;
		marked[x]++;
		if(marked[x] > 2) flag = false;
	}
	int cnt1 = 0, cnt2 = 0;
	for(int i = 0; i <= 200000; ++i)
		if(marked[i] > 0) marked[i]--, s1.push_back(i);
	for(int i = 200000; i >= 0 ; --i)
		if(marked[i] > 0) s2.push_back(i);

	if(flag)
	{
		cout << "YES\n";
		cout << s1.size() << '\n';
		for(int i = 0 ; i < s1.size(); ++i) cout << s1[i] << ' ';
			cout << '\n';
		cout << s2.size() << '\n';
		for(int i = 0 ; i < s2.size(); ++i) cout << s2[i] << ' ';
		cout << '\n';
	}
	else cout << "NO\n";
	return 0;
}