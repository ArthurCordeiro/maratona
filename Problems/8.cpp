#include <bits/stdc++.h>
#define MAXN 200005
#define bug(x) cout << #x << ' ' << x << '\n'
using namespace std;
vector<pair<int , pair<int, int> >> operation, operation2;
int a[MAXN], times[MAXN], indice;
int main()
{
	int n;
	pair<int, int> v = {0, 0}; 
	cin >> n;
	for(int i = 0 ; i < n; ++i)
	{
		cin >> a[i];
		times[a[i]]++;
		if(times[a[i]] >= v.first) v = {times[a[i]], a[i]}, indice = i;
	}
	//bug(v.first);bug(v.second);
	for(int i = 0; i < indice; ++i)
	{
		while(a[i] != v.second)
		{
			//bug(a[i]); bug(i);
			if(a[i] > v.second) 
			{
				a[i] -= abs(a[i] - v.second);
				operation.push_back({2, {i+1, i + 2}});
				//bug(a[i]);
			}
			else 
			{
				a[i] += abs(a[i] - v.second);
				operation.push_back({1, {i+1, i + 2}});
				//bug(a[i]);
			}
		}
	}
	for(int i = indice + 1; i < n; ++i)
	{
		while(a[i] != v.second)
		{
			//bug(a[i]); bug(i);
			if(a[i] > v.second) 
			{
				a[i] -= abs(a[i] - v.second);
				operation2.push_back({2, {i + 1, i}});
				//bug(a[i]);
			}
			else 
			{
				a[i] += abs(a[i] - v.second);
				operation2.push_back({1, {i + 1, i}});
				//bug(a[i]);
			}
		}
	}

	cout << operation.size() + operation2.size()<< '\n';
	for(int i = operation.size() - 1; i>=0; --i)
		cout << operation[i].first << ' ' << operation[i].second.first << ' ' << operation[i].second.second << '\n';
	for(auto u : operation2)
		cout << u.first << ' ' <<  u.second.first << ' ' << u.second.second << '\n';
	return 0;
}
