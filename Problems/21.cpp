#include <bits/stdc++.h>

using namespace std;


string s, wd;
int n;

int main()
{
	cin >> n >> s;
	wd = "ACTG";	
	int aux ,ans = 100000000;
	for(int i = 0 ;  i < n - 3; ++i)
	{
		aux = 0;
		for(int j = 0 ; j < 4; ++j)
			aux += min( abs(s[i+j] - 'Z') + wd[j] - 'A' + 1, min(s[i+j] - 'A' + abs(wd[j] - 'Z') + 1, abs(s[i+j] - wd[j])));  	
		ans = min(aux, ans);
	}
	cout << ans;		
}