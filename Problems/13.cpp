#include <bits/stdc++.h>

using namespace std;

int main()
{
	string s; cin >> s;
	int n = s.size();
	int r = 0;
	vector<int> mod(3, -1);
	vector<int> pd(n + 1);
	mod[0] = 0;
	for(int i = 1; i <= n; i++)
	{
		r = (r + s[i - 1] - '0') % 3;
		pd[i] = pd[i - 1];
		if(mod[r] != -1)
			pd[i] = max(pd[i - 1], pd[mod[r]] + 1);
		mod[r] = i;
	}
	cout << pd[n];
	return 0;
}