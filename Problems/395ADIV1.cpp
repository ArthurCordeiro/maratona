#include <bits/stdc++.h>
#define MAXN 100005
using namespace std;
int n;
vector<int> G[MAXN];
int color[MAXN];
int main()
{
	cin >> n;
	for(int i = 0 ; i < n -1 ; ++i)
	{int a, b; cin >> a >> b;}
	for(int i = 0 ; i < n; ++i) cin >> color[i];
	int resp = 0, cnt = 0;
	for(int i = 1; i <= n - 2; ++i)
	{	
		if(color[i] != color[i-1] && color[i-1] == color[i + 1]) {cnt++, resp = i, i++; continue;}
		if(color[i - 1] != color[i+1]) cnt++, resp = i;
		if(cnt > 1){cout << "NO"; return 0;}
	}
	cout << "YES\n";
	if(cnt) cout << resp + 1;
	else cout << n/2;
	return 0;
}
