#include<cstdio>
#include <cstring>
#define MAXN 100100
#define MAXT 55
using namespace std;
typedef long unsigned int ll;
long long memo[MAXN][MAXT];
ll N,M;
ll MOD = 1000000007;

ll maneiras(ll id,ll tempo){
	if(tempo==1) return 1;

	//cout<<id<<" "<<tempo<<endl;
	if(memo[id][tempo]!=-1) return memo[id][tempo];
	
	ll resp=0;
	if(id<M){
		resp+=(maneiras(id+1,tempo-1)%MOD);
	}
	if(id>N){
		resp+=(maneiras(id-1,tempo-1)%MOD);
	}
	resp=resp%MOD;
	return memo[id][tempo]=resp;
	
}


int main(){
	ll T;
	scanf("%lu%lu%lu",&T,&N,&M);
	memset(memo,-1,sizeof(memo));
	ll res=0;
	for(ll i=N;i<=M;i++){res+=(maneiras(i,T)%MOD);	
	res=res%MOD;
	}
	res=res%MOD;
	
	printf("%lu",res);
	return 0;
}
