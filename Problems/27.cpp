/*#include <bits/stdc++.h>
#define MAXN 400005
using namespace std;
int a[MAXN], lefti[MAXN], righti[MAXN], n;
vector<char> s;
int main()
{
	cin >> n;
	for(int i = 0; i < n; ++i) lefti[i] = 1, righti[i] = 1;
	for(int i = 0 ; i < n ; ++i) cin >> a[i];
	for(int i = 1 ; i < n; ++i) if(a[i-1] > a[i]) righti[i] = righti[i-1] + 1;
	for(int i = n -2; i >= 0; --i) if(a[i+1] > a[i]) lefti[i] = lefti[i-1] + 1;
	int l = 0, r = n - 1, ans = 0, ans1 = 0, ans2= 0;
	for(l = 0, r = n -1; l!= r; )
	{
		if(a[l] < a[r]){s.push_back('L'); l++;}
		else if(a[r] < a[l]){s.push_back('R'); r--;} 
		else
		{	
			for(int j = l; j < n - 1; ++j) if(a[j] < a[j+1])ans1++;
			for(int j = r; j >= 1; --j)  if(a[j] < a[j-1])ans2++;
			ans1++, ans2++;
			if(ans1 >= ans2) while(ans1--) s.push_back('L');
			else while(ans2--) s.push_back('R');
			break;
		}
	}
	//cout << l << ' ' << r << '\n';
	if(l == r) s.push_back('R');
	cout << s.size() << '\n';
	for(auto u : s) cout <<u;
	return 0;
}
*/
#include <bits/stdc++.h>
using namespace std;
#define DIM 300009
long long i,j,k,l,n,m,flag,r,last;

long long a[DIM],dpl[DIM],dpr[DIM];

string res;

int main()
{

    cin>>n;
    for(i=1; i<=n; i++)
    {
        cin>>a[i];
        if(a[i]<a[i-1])dpr[i]=dpr[i-1]+1;
        else dpr[i]=1;
    }

    for(i=n;i>0;i--)
        if(a[i]<a[i+1])dpl[i]=dpl[i+1]+1;
        else dpl[i]=1;
    

    l=1;
    r=n;

    last=0;

    for(i=1; i<=n; i++)
    {
        if(max(a[l],a[r])<=last)
            break;

        if(a[l]!=a[r])
        {
            if(min(a[l],a[r])>last)
            {
                if(a[l]<a[r])
                    res+='L',last=a[l],l++;
                else
                    res+='R',last=a[r],r--;
            }
            else
            {
                if(max(a[l],a[r])>last)
                {
                    if(a[l]>a[r])
                        res+='L',last=a[l],l++;
                    else
                        res+='R',last=a[r],r--;
                }
            }
        }
        else{
            if(dpl[l]>dpr[r])
            {
                for(j=1;j<=dpl[l];j++)res+='L';
                break;
            }
            else
            {
                for(j=1;j<=dpr[r];j++)res+='R';
                break;
            }
        }
    }

    cout<<res.length()<<endl;
    cout<<res<<endl;
	return 0;
}
