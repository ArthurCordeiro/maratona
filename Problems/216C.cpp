#include <bits/stdc++.h> 
#define MAXN 100005
using namespace std;


int n, pai[MAXN], h[MAXN];
vector<pair<int, int>> G[MAXN];

vector<pair<int, pair<int, int>>>altura[MAXN];
bool marked[MAXN], visited[MAXN];
void dfs(int u)
{
	marked[u] = true;
	for(auto v : G[u])
		if(!visited[v.first])
		{
			h[v.first] = 1 + h[u];
			altura.push_back({1 + h[u] ,{v.second, v.first}});	
			pai[v.first] = u;
			dfs(v.first);
		}
}
int main()
{
	cin >> n;
	for(int i = 1; i<=n; ++i) h[i] = 0, pai[i] = i;
	
	for(int i = 1; i <= n -1; ++i)
	{
		int a, b, c; cin >> a >> b >> c; 
		G[a].push_back({b, c});
	}
	h[1] = 1;
	dfs(1);
	
	sort(altura.begin(), altura.end());	
	reverse(altura.bengin(), altura.end());
	
	int k = altura.size();
	vector<int> q;
	for(auto i : altura)
		if(!visited[i.second.second] && i.second.first == 2)
		{
			int x = i.second.second;
			q.push_back(x);
			while(pai[x] != 1)
			{
				visited[x] = true;
				x = pai[x];
			}
		
		}
	
	for(int i : q)
		cout << i << ' ';
		
	return 0;
}
