#include <bits/stdc++.h>
#define MAXN 100005
using namespace std;
vector<int> G[MAXN], resp;
int n; bool state1[MAXN], state2[MAXN];
void dfs(int u, bool flag)
{
	if(!flag )
		state1[u] != state1[u];
	for(int i : G[u])
		dfs(i, !flag);
}

int main()
{
	cin >> n;
	for(int i = 0 ; i < n - 1; ++i)
	{
		int a,b; cin >> a >> b;
		G[a].push_back(b);
	}
	for(int i = 1; i <= n; ++i) cin >> state1[i];
	for(int i = 1; i <= n; ++i) cin >> state2[i];
	
	for(int i = 1; i <= n; ++i)
		if(state1[i] != state2[i])
			resp.push_back(i),dfs(i, false);
	cout << resp.size() << '\n';
	for(int i : resp)
		cout << i << '\n';			
	return 0;
}
