/*#include <bits/stdc++.h>

using namespace std;

char matrix1[25][25], matrix2[25][25];
bool marked[25][25];
int n, m;
void flood(int i, int j, char c1, char c2)
{
	if(i >  n || j > m || i < 1 || j < 1) return;
	if(marked[i][j]) return;
	marked[i][j] = true;
	if(c1 == '>') flood(i, j+1, matrix1[1][j+1], matrix2[i][j+1]);
	else flood(i, j - 1, matrix1[i ][j-1], matrix2[i][j-1]);
	if(c1 == 'v') flood(i + 1, j, matrix1[i +1][j], matrix2[i+1][j]);
	else flood(i - 1, j, matrix1[i -1][j], matrix2[i-1][j]);
}
int main()
{
	cin >> n >> m;
	for(int i = 1 ; i <= n; ++i) cin >> matrix1[i][1];
	for(int i = 1 ; i <= m; ++i) cin >> matrix2[1][i];	
	for(int i = 1; i<= n; ++i)
	{
		for(int j = 1; j <= m ; ++j)
			if(matrix1[i][1] == '>') matrix1[i][j] = '>';
			else matrix1[i][j] = '<';
	
	}	
	for(int i = 1; i<= n; ++i)
	{
		for(int j = 1; j <= m ; ++j)
			if(matrix1[1][j] == '^') matrix2[j][i] = '^';
			else matrix2[i][j] = 'v';
	}
	flood(1, 1, matrix1[1][1], matrix2[1][1]);
	for(int i = 1; i <= n; ++i)
		for(int j = 1; j<= m; ++j)
			if(!marked[i][j]){cout << "NO";return 0;}
	cout << "YES";			
	return 0;
}*/

#include <bits/stdc++.h>


using namespace std;
string s1, s2, s3;
int n, m;
int main()
{
	
	cin >> n >> m >>s1 >> s2;
	s3 = {s1.front(), s1.back(), s2.front(), s2.back()};
	cout << (s3 == "><^v" || s3 == "<>v^" ? "YES" : "NO");
	return 0;
}
