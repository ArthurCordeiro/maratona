#include <iostream>
#include <stdio.h>
using namespace std;

const int N = 1e5 + 10;

int n, color, ans, mx, f[N], cnt[N];

int main()
{
	scanf("%d", &n);
	for (int i = 1; i <= n; i++)
    {
        scanf("%d", &color);
        cnt[f[color]]--;
        f[color]++;
        cnt[f[color]]++;
        mx = max(mx, f[color]);
        bool ok = false;
        if (cnt[1] == i) 
            ok = true;
        else if (cnt[i] == 1) 
            ok = true;
        else if (cnt[1] == 1 && cnt[mx] * mx == i - 1) 
            ok = true;
        else if (cnt[mx - 1] * (mx - 1) == i - mx && cnt[mx] == 1) /
            ok = true;
        if (ok)
            ans = i;
    }
    printf("%d", ans);
	return 0;
}
