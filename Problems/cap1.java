class Tree 
{   Tree left; String key; Tree right; Object Bind;
    Tree(Tree l, String k, Object bind,Tree r) {left=l; key=k; Bind = bind;right=r;}
}

Tree insert(String key, Tree t, Object bind) 
{
    if (t==null) return new Tree(null, key, bind, null)
    else 
        if (key.compareTo(t.key) < 0)
            return new Tree(insert(key, bind,t.left),t.key,t.right);
        else 
            if (key.compareTo(t.key) > 0)
                return new Tree(t.left,t.key,insert(key,bind,t.right));
            else 
                return new Tree(t.left,key, bind,t.right);
}


Tree find(String key, Tree t)
{
    if(t.key == key) return true;
    if(t == null) return false;

    return find(key, t.left) || find(key, t.right);             
}
