#include <bits/stdc++.h>

using namespace std;

vector<int> v[60];
int main()
{
	int n, k, qmax = 0;
	cin >> n >> k;
	for(int i = 0 ; i < n ; ++i)
	{
		int a, b; cin >> a >> b;
		v[a].push_back(b);
		qmax= max(qmax, a);
	}
	int position = 0, q, t;
	for(int i = qmax; i >= 1; --i)
	{
		sort(v[i].begin(), v[i].end());
		for(auto x : v[i]) 
		{	
			position++;
			if(position == k) 
			{q = i; t = x; break;}
		}
		if(position == k) break;
	}
	int resp = 0;
	for(auto x : v[q])
		if(x == t) resp++;
	cout << resp;
	return 0;
}