#include <bits/stdc++.h>
#define MAXN 100005
using namespace std;
int n, k, a[MAXN], b[MAXN];
int main()
{
	cin >> n >> k;
	for(int i = 1 ; i <= n; ++i)a[i] = k +1, b[i] = -1;
	for(int i = 1; i <= k; ++i)
	{
		int x; cin >> x;
		a[x] = min(a[x], i);
		b[x] = max(b[x], i);
	}
	int ans = 0;
	for(int i = 1; i <= n; ++i)
	{
		if(i > 1 && b[i-1] <= a[i]) ans++;
		if(i < n && b[i +1] <= a[i]) ans++;
		if(b[i] == -1) ans++;
	}
	cout << ans; 
	return 0;
}
