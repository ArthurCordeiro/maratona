#include <bits/stdc++.h>
#define MAXN 100005
using namespace std;
vector<int> G[MAXN];
bool visited[MAXN];
int n, m;
int dfs(int u)
{
	cout << u << ' ';
	visited[u] = true;
	for(auto v : G[u])
	{
		if(!visited[v])
			dfs(v);
		
	}
	cout << u << ' ';
	
}

int main()
{
	cin >> n >> m;
	for(int i = 0; i < m; ++i)
	{	
		int a, b, c; cin >> a >> b ;
		G[a].push_back(b);G[b].push_back(a);
	}
	dfs(1);
	return 0;	
}

