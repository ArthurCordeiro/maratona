/*#include <bits/stdc++.h>
#define MAXN 100005

using namespace std;

vector<int> v;
int n, t, c, a[MAXN];

bool jump(int x, int c )
{
    int count = 1;
    //vector<int>::iterator k;
    //k = v.begin();
    for(int i = 0; i < v.size(); ++i)
    {
        int temp = v[i] + x;
        auto k = lower_bound(v.begin()+i, v.end(), temp);
        if(k == v.end()) break;
        else i = k - v.begin() - 1, count++;
    }   
    
    if(count == c) return true;
    return false;
}
int main()
{
    cin >> t;
    while(t--)
    {
        int x;
        cin >> n >> c;
        for(int i = 0; i < n; ++i) cin >> x , v.push_back(x);

        sort(v.begin(), v.end());
        int ini = 1, fim = 10000000, mid, resp = -1;        
        while(ini <= fim)
        {
            mid = (ini + fim)/2;
            if(jump(mid, c)) resp = max(mid, resp), ini = mid + 1;
            else fim = mid -1;
        }
        cout << resp << '\n';
        v.clear();
    }
     return 0;
}
*/


#include <bits/stdc++.h>
using namespace std;
int n,c;
int func(int num,int array[])
{
    int cows=1,pos=array[0];
    for (int i=1; i<n; i++)
    {
        if (array[i]-pos>=num)
        {
            pos=array[i];
            cows++;
            if (cows==c)
                return 1;
        }
    }
    return 0;
}
int bs(int array[])
{
    int ini=0,last=array[n-1],max=-1;
    while (last>ini)
    {
        //cout<<last<<" "<<ini<<endl;
        int mid=(ini+last)/2;
        if (func(mid,array)==1)
        {
            //cout<<mid<<endl;
            if (mid>max)
                max=mid;
            ini=mid+1;
        }
        else
            last=mid;
    }
    return max;
}
int main()
{
    int t;
    scanf("%d",&t);
    while (t--)
    {
        scanf("%d %d",&n,&c);
        int array[n];
        for (int i=0; i<n; i++)
            scanf("%d",&array[i]);
        sort(array,array+n);
        //cout<<" dfsa \n";
        int k=bs(array);
        printf("%d\n",k);
    }
    return 0;
}
