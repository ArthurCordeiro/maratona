#include <bits/stdc++.h>
#define bug(x) cout << #x << ' ' << x << '\n'
using namespace std;
vector<int> q; bool marked[200];
int main()
{
	string s; int n; cin >> n;
	while(n--)
	{
		bool flag = true;
		cin >> s;
		for(int i = 0 ; i < s.size(); ++i)
		{	
			int x = (int)s[i];
			q.push_back(x);	
			//bug((int)s[i]);
			if(marked[x]) flag = false;
			marked[x] = true;		
		}
		sort(q.begin(), q.end());
		for(int i = 0; i < q.size() - 1; ++i)
			if((int)q[i+1] != (int)(q[i] + 1)){flag = false;}
		

		if(flag && q.size() == s.size()) cout << "YES\n";
		else cout << "NO\n";
		q.clear();
		for(int i = 'a'; i <= 'z'; ++i) marked[i] = false;
	}

	return 0;
}