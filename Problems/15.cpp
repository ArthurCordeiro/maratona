#include <bits/stdc++.h>
#define MAXN 100000000
#define bug(x, y) cout << #x << ' ' << x << '\n' << #y <<' ' << y << '\n'
#define print(x) cout << #x << ' ' << x << '\n'
using namespace std;


int n, r1, c1, r2, c2; 
vector<pair<int, int>> G1, G2;
string table[55];
bool marked[55][55];

void floodfill(int i, int j, bool flag)
{
	if(i < 0 || i == n) return;
	if(j < 0 || j == n) return;  
	if(table[i][j] == '1') return;
	if(marked[i][j]) return;
	marked[i][j] = true;
	(flag) ? G1.push_back(make_pair(i, j)) : G2.push_back(make_pair(i, j));
	floodfill(i+1, j, flag);floodfill(i-1, j, flag);floodfill(i, j - 1, flag);floodfill(i, j + 1, flag);
}
int main()
{
	cin >> n >> r1 >> c1 >> r2 >> c2;	
	r1--,c1--;r2--,c2--;
	for(int i = 0 ; i < n ; ++i) cin >> table[i];
	if(table[r1][c1] == '1') G1.push_back(make_pair(r1, c1)), marked[r1][c1]; 
	if(table[r2][c2] == '1') G2.push_back(make_pair(r2, c2)), marked[r2][c2];
	floodfill(r1,c1, true); floodfill(r2, c2, false);
	if(G2.size() == 0){cout << 0; return 0;}
	int ans = MAXN;
	for(auto u : G1)
		for(auto v : G2)
			{int var = (u.first - v.first)*(u.first - v.first) + (u.second-v.second)*(u.second-v.second); ans = min(ans, var);}
		
	cout << ans;
	return 0;
}