#include <bits/stdc++.h>
using namespace std;

int t, n; string s;
int main()
{
	cin >> n >> t >> s;
	for(int i = 0; i < t; ++i)
		for(int j = n - 1; j >= 1; --j)
			if(s[j] == 'G' && s[j - 1] == 'B') swap(s[j],s[j-1]), --j;
	cout << s;
	return 0;		
}
