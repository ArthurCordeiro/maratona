
#include <bits/stdc++.h>
#define MAXN 100005
using namespace std;

int a[MAXN];
int main()
{
    bool flag = true;
    int n;cin >> n;
    for(int i = 0 ; i < n ; ++i)
    {   
        int x; cin >> x;
        if(x == 25) a[x]++;
        if(x == 50)
        {
            if(a[25] > 0) a[25]--;
            else flag = false;
            a[50]++;
        }
        if(x == 100)
        {
            if(a[25] > 0 && a[50] > 0) a[25]--, a[50]--;
            else if(a[25] >= 3) a[25] -= 3;
                else flag = false;
        }

    }
    if(flag) cout <<"YES";
    else cout << "NO";
    return 0;   
}
