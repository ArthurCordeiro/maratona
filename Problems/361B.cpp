/*#include <bits/stdc++.h>
#define MAXN 100005
using namespace std;
int dist[MAXN], n, m;
bool visited[MAXN];
vector<pair<int , int>> G[MAXN];
void dijkstra(int x)
{
	priority_queue<pair<int,int>> q;
	for(int i = 1; i <= n; ++i) dist[i] = MAXN;
	dist[x] = 0;
	q.push({0, x});
	
	while(!q.empty())
	{
		int v = q.top().second; q.pop();
		if(visited[v]) continue;
		visited[v] = true;
		for(auto u : G[v])
		{
			int atual = u.first, w = u.second;  
			if(dist[v] + w < dist[atual])
			{
				dist[atual] = dist[v] + w;
				q.push({-dist[atual], atual});
			}
		}
	}
}
int main()
{
	cin >> n;
	for(int i = 1; i <= n; ++i)
	{
		int j; cin >> j; G[i].push_back({j, 1});
	}
	dijkstra(1);
	cout << 0 << ' ';
	int temp = 1;
	for(int i = 2; i <= n; ++i)
	{
		if(dist[i] == 1){temp = i; cout << 1 << ' ';}
		else cout << abs(temp - i) << ' ';
	}
	return 0;	
}*/

    #include <stdio.h>
    #include <iostream>
    #include <vector>
    #include <string.h>
    using namespace std;
     
    #define maxn 200005
    #define INF 0x3f3f3f3f
     
    int n;
    vector <int> seq[maxn];
    int dis[maxn];
     
    void dfs(int s)
    {
    	for (int i = 0; i < seq[s].size(); i++)
    	{
    		int cur = seq[s][i];
    		if (dis[cur] > dis[s] + 1)
    		{
    			dis[cur] = dis[s] + 1;
    			dfs(cur);
    		}
    	}
    }
     
    int main()
    {
    	cin >> n;
    	for (int i = 1;i <= n; i++)
    	{
    		int x;
    		cin >> x;
    		seq[i].push_back(x);
    		if (i + 1 <= n)
    			seq[i].push_back(i + 1);
    		if (i - 1 >= 1)
    			seq[i].push_back(i - 1);
    	}
     
    	for (int i = 1; i <= n; i++)
    		dis[i] = INF;
    	dis[1] = 0;
    	dfs(1);
    	for (int i = 1; i <= n; i++)
    		cout << dis[i] << " ";
    	cout << endl;
    	return 0;
    }
