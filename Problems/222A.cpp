#include <bits/stdc++.h>

using namespace std;
int n, m, k, walk[][2] = {{1,0},{-1,0},{0,-1},{0,1}}, visited[1010][1010] = {0};
char matrix[1010][1010];

void dfs(int i, int j)
{
	if(i >= n || i < 0 || j >= m || j < 0) return ;
	if(matrix[i][j] != '.') return ;
	if(visited[i][j]) return;	
	visited[i][j] = 1;
	for(int k = 0 ; k < 4; ++k)
	{
		int dx = i + walk[k][0],
			dy = j + walk[k][1];
			dfs(dx, dy);		
	}
	if(k)
		k--, matrix[i][j] = 'X';	
}

int main()
{
	cin >> n >> m>> k;
	for(int i = 0 ; i < n; ++i)
		cin >> matrix[i];
	cout << '\n';	
	for(int i = 0; i < n; ++i)
		for(int j = 0; j < m; ++j)
			dfs(i, j);
	for(int i = 0; i < n; ++i)
		cout << matrix[i] << '\n';
	return 0;
}
