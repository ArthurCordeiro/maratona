#include <bits/stdc++.h>
#define MAXN 100005
using namespace std;
int n; 
vector<int> c;
long long ans;
int main()
{
	cin >> n; ans = 0;
	for(int i = 0 ; i < n ; ++i)
	{
		int a, b; cin >> a >> b;
		c.push_back(a-b);
		ans += (b*n - a);
	}		
	sort(c.begin(),c.end());
	int j = 1;
	for(int i = c.size() - 1;  i >= 0; --i)
		ans += 1LL*c[i]*j, j++ ; 
	cout << ans;
	return 0;
}