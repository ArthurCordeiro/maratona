#include <bits/stdc++.h>
#define MAXN 55
using namespace std;
int n, h, m, r[MAXN], l[MAXN], x[MAXN], altura[MAXN];
int main()
{
	cin >> n >> h >> m;
	for(int i = 1; i <= m; ++i)
		cin >> l[i] >> r[i] >> x[i];		
	for(int i = 1 ; i <= n; ++i) altura[i] = h;
	for(int i = 1 ; i <= m; ++i)
		for(int j = l[i]; j<=r[i]; j++)
			altura[j] = min(altura[j], x[i]);
	
	int resp = 0;
	for(int i = 1; i <= n; ++i) resp += (altura[i]*altura[i]);
	cout << resp;
	return 0;
}
