#include <bits/stdc++.h>

using namespace std;

int n, m, table[505][505], c[505];
void PRINT()
{
	cout << "TAK\n";
	for(int i = 0; i < n; ++i)
		cout << c[i] << ' ';
}
int main()
{
	cin >> n >> m;
	for(int i = 0 ; i < n ; ++i)
		for(int j = 0; j < m; ++j)
			cin >> table[i][j];
	int sum = 0;
	for(int i = 0; i < n; ++i)
		sum ^= table[i][0], c[i] = 1;
	if(sum){PRINT(); return 0;}
	else
	{
		for(int i = 0 ; i < n; ++i)
			for(int j = 1; j < m; ++j)
				if(table[i][j] != table[i][0])
				{c[i] = j + 1; PRINT(); return 0;}
	}
	cout << "NIE\n";
	return 0;
}
