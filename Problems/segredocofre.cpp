#include <bits/stdc++.h>
#define MAXN 100005
using namespace std;

int n, m, ps[MAXN], s[MAXN], num[10];
void update(int l, int r, int x)
{
	ps[l] += x;
	ps[r+1]-=x;
}
int main()
{
	cin >> n >> m;
	for(int i = 1 ; i <= n; ++i) cin >> s[i];
	int last = 1;
	update(1, 1, 1);
	for(int i = 1 ; i <= m ; ++i)
	{
		int next; cin >> next;
		int l = min(next, last), r = max(next, last);
		update(l, r, 1), update(l, l, -1);
		last = next;
	}
	for(int i = 1; i <= n; ++i)
	{
		ps[i] += ps[i-1];
		num[s[i]] += ps[i];
	}
	for(int i = 0 ; i < 9; ++i) cout << num[i] << ' ';
	cout << num[9] << '\n';
	return 0;
}
