#include <bits/stdc++.h>

using namespace std;
const int N = 1e5;  // limit for array size

int t1[2 * N], t2[2*N],  n;

void build1() 
{  
// build the tree
  for (int i = n - 1; i > 0; --i) t1[i] = t1[i<<1] + t1[i<<1|1];
}
void build2() 
{  
// build the tree
  for (int i = n - 1; i > 0; --i) t2[i] = t2[i<<1] + t2[i<<1|1];
}
void modify1(int p, int value) 
{  // set value at position p
  for (t1[p += n] = value; p > 1; p >>= 1) t1[p>>1] = t1[p] + t1[p^1];
}
void modify2(int p, int value) 
{  // set value at position p
  for (t2[p += n] = value; p > 1; p >>= 1) t2[p>>1] = t2[p] + t2[p^1];
}
int query1(int l, int r) 
{	// sum on interval [l, r)
  int res = 0;
  for (l += n, r += n; l < r; l >>= 1, r >>= 1) 
  {
    if (l&1) res += t1[l++];
    if (r&1) res += t1[--r];
  }
  return res;
}
int query2(int l, int r) 
{	// sum on interval [l, r)
  int res = 0;
  for (l += n, r += n; l < r; l >>= 1, r >>= 1) 
  {
    if (l&1) res += t2[l++];
    if (r&1) res += t2[--r];
  }
  return res;
}
int main() 
{
	scanf("%d", &n);
  for (int i = 0; i < n; ++i)
	{
		int x; cin >> x; 
		if(x&1) t1[n + i]++;
		else t2[n + i]++;
	}
  build1();
  build2();
  int q; cin >> q;
  for(int i = 0 ; i < q; ++i)
  {
  	int o, x , y; cin >> o >> x >> y;
  	if(o == 0)
  	{
  		if(y&1 && t1[x-1] == 0) modify1(x-1, 1), modify2(x-1, 0);
  		if(y%2 == 0 && t2[x-1] == 0) modify2(x-1, 1), modify1(x-1, 0);
  	}
  	if(o == 1) cout << query2(x-1, y-1) << '\n'; 
  	if(o == 2) cout << query1(x-1, y-1) << '\n';
  }
  for(int i  = 0 ; i < 6; ++i) cout << t2[i] << ' ';
  return 0;
}
