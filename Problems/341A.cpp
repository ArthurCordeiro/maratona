#include <bits/stdc++.h>
#define MAXN 100005
using namespace std;
long long n, a[MAXN];
int main()
{
	cin >> n;
	int impar = 0, par = 0;
	for(int i = 0 ; i < n ; ++i) 
	{
		cin >> a[i];
		(a[i]&1)? impar++: par++;
	}
	sort(a, a +n);
	long long sum = 0;
	if(impar&1)
	{
		for(int i = n -1; i >=0; --i)
		{
			if(impar == 1 && a[i] & 1) continue;
			if(a[i] & 1) sum+=a[i], impar--;
			else sum+=a[i];
		}
	}
	else
		for(int i = n -1; i >=0; --i)
			sum += a[i];	
	cout << sum;
}
