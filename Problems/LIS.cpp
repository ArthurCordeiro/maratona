#include <bits/stdc++.h>
#define MAXN 300005
using namespace std;
int n;
char s[MAXN];
string p;
int main()
{
	cin >> s;
	n = strlen(s);
	for(int i = 0 ; i < n; ++i)
	{
		char x = s[i];
		string ::iterator it = upper_bound(p.begin(), p.end(), x);
		if(it == p.end()) p += x;
		else *it = x;
	}
	cout << p.size() << '\n';
	return 0;
}
