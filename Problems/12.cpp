#include <bits/stdc++.h>
#define MAXN 
using namespace std;
int n, a[105], h[105];
int marked[105];
int main()
{
	string s;
	cin >> n;
	int t1 = 0, t2 = 0, t3 = 0;
	for(int i = 0 ; i < n ; ++i)
		cin >> a[i], h[a[i]]++;
	for(int i = 1; i <= 100; ++i)
	{
		if(h[i] == 1) t1++;
		if(h[i] == 2) t2++;
		if(h[i] > 2) t3++;
	}
	bool flag = true;
	if(t1%2 == 0)
	{
		cout << "YES\n";
		for(int i = 0; i < n; ++i)
		{	
			if(h[a[i]] == 1) flag = !flag;
			cout << ((flag) ? 'A' : 'B');
		}
	}
	else if(t3 > 0)
	{
		bool flag2 = true;
		cout << "YES\n";
		for(int i = 0; i < n; ++i)
		{	
			if(h[a[i]] > 2 && flag2) 
			{
				flag2 = false; 
				cout << (flag ? 'A' : 'B'); 
				marked[a[i]] = (flag) ? 2 : 1;
			}
			if(marked[a[i]]) {cout << ((marked[a[i]] == 1) ? 'A' : 'B'); continue;} 	
			if(h[a[i]] == 1) flag = !flag;
			cout << (flag ? 'A' : 'B');
		}	
	}
	else cout << "NO\n";
	return 0;
}