#include <bits/stdc++.h>

using namespace std;
int walk[][2] = {{1, 0}, {0, 1}};
char matrix[55][55];
bool visited[55][55];
int n, m;
bool flag;
void floodfill(int i, int j, char c)
{
	if(i >= n || i < 0 || j >= m || j < 0) return;	
	if(c != matrix[i][j]) return;
	if(visited[i][j]){flag = true; return;}
	visited[i][j] = true;
	for(int i = 0 ; i < 2; ++i)
	{
		int dx = i + walk[i][0];
		int dy = j + walk[i][1];
		floodfill(i + dx, j + dy, matrix[i][j]);
	}	

}
int main()
{
	cin >> n >> m;
	for(int i = 0 ; i < n; ++i)
		cin >> matrix[i]; 
	flag = false;
	for(int i = 0 ; i < n; ++i)
		for(int j = 0 ; j < m ; ++j)
		{	
			memset(visited, 0, sizeof(visited));
			floodfill(i, j, matrix[i][j]);
			if(flag){cout << "Yes"; return 0;} 
		}
	cout << "No";	
	return 0;
}
