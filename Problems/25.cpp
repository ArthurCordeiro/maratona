#include <bits/stdc++.h>

using namespace std;
long long n;
map<long long, bool> c;
int solve(long long x)
{
	if(c[x]) return 0;
	else
	{
		c[x] = true;
		x++;
		while(x%10 == 0) x /= 10;
		//c[x] = true;
		return 1 + solve(x);
	}
}
int main()
{
	cin >> n;
	
	cout << solve(n) << '\n';
	return 0;
}
