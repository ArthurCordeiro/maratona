#include <bits/stdc++.h>
#define MAXN 100005
#define print(x) cout << #x << ' ' << x << '\n'
using namespace std;
vector<int> G[MAXN];
int n, m;
bool marked[MAXN];
int main()
{
	cin >> n >> m;
	for(int i = 0; i < m; ++i)
	{ 
		int u, v; cin >> u >> v;
		G[u].push_back(v); G[v].push_back(u);
	}
	
	priority_queue<int> q;
	for(int i = 1; i <= n; ++i)
	{
		if(!marked[i])
		{
			q.push(i);
			marked[i] = true;			
			while(!q.empty())
			{
				int v = abs(q.top()); q.pop();
				for(auto u : G[v])
					if(!marked[u]) marked[u] = true, q.push(-u);
				cout << v << ' ';
			}
		}
	} 
	return 0;
}