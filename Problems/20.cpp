#include <bits/stdc++.h>
#define MAXN 200005
#define bug(x) cout << #x << ' ' << x << '\n'
using namespace std;
double a[MAXN], b[MAXN], n;
vector<int> G;
map<long double,int> c;

int main()
{
	cin >> n;
	for(int i = 0; i < n; ++i)
		cin >> a[i];
	for(int i = 0; i < n; ++i)
		cin >> b[i];
	int aux = 0, ans = 0;
	for(int i = 0; i < n; ++i)
	{
		if(a[i]!=0) ++c[-b[i]/a[i]];
		if(a[i]==0 && b[i] == 0) aux++;
	}
	for(auto v : c)
		ans= max(v.second, ans);
	cout << ans + aux;
	return 0;
}