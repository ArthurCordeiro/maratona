#include <bits/stdc++.h>

using namespace std;

int main()
{
	string s; cin >> s;
	int ct = 0;
	for(int i = 0 ; i < s.size(); ++i)
		if(s[i] == 'a') ct++;
	int var = s.size() -  2*ct + 1;
	if(var > 0) cout << 2*ct - 1;
	else cout << s.size();
	return 0;
}