#include <cstdio> 
#include <algorithm> 

using namespace std;

#define MAXN 100100 


int n, m, v[MAXN], f[MAXN];


long long dig[10];

void upd(int l, int r, int d)
{
  
  f[l]+=d;
  f[r+1]-=d;
}

int main()
{
  scanf("%d %d", &n, &m);
  for(int i=1;i<=n;i++) scanf("%d", &v[i]);
  
  int last=1;
  upd(1,1,1);
  
  for(int i=1;i<=m;i++)
  {
    int next;
    scanf("%d", &next);
    int l=min(last,next), r=max(last,next);
    upd(l,r,1);
    upd(last,last,-1);
    last=next;
  }
  
  for(int i=1;i<=n;i++)
  {
    f[i]+=f[i-1];
    dig[v[i]]+=f[i];
  }
  
  for(int i=0;i<10;i++) printf("%d ", dig[i]);
  printf("\n");
  
  return 0;
}
