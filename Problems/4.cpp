#include <bits/stdc++.h>
#define bug(x) cout << #x << ' ' << x << '\n'
using namespace std;
long long n;
long long solve(long long n)
{
	
	if(n == 0) return 1;
	else if(n < 10) return n;
	return max(solve(n/10)*(n%10), solve((n/10)-1)*9);
}

int main()
{
	cin >> n;
	cout << solve(n);
	return 0;
}