#include <bits/stdc++.h>

using namespace std;
#define int long long
main()
{
	int a, b, c, d; cin >> a >> b >> c >> d;
	int maximo = max(a, max(b, max(c, d)));
	if(abs(a - maximo)) cout << abs(a - maximo) << ' ';
	if(abs(b - maximo)) cout << abs(b - maximo) << ' ';
	if(abs(c - maximo)) cout << abs(c - maximo) << ' ';
	if(abs(d - maximo)) cout << abs(d - maximo) << ' ';
	return 0;
}
