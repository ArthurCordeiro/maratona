#include <bits/stdc++.h>
#define INF 9999999
using namespace std;
vector<pair<int, int>> G[255];
int dist[255];
int s, c;
bool visited[255];
void djikstra(int x)
{
	priority_queue<pair<int, int>> q;
	for (int i = 1; i <= s; i++) dist[i] = INF,visited[i]=false;
	dist[x] = 0;
	q.push({0,x});
	while (!q.empty()) 
	{
		int a = q.top().second; q.pop();
		if (visited[a]) continue;
		visited[a] = true;
		for (auto u : G[a]) 
		{
			int b = u.first, w = u.second;
			if (dist[a]+w < dist[b]) 
			{
				dist[b] = dist[a]+w;
				q.push({-dist[b],b});
			}
		}	
	}
}
int main()
{
	cin >> s >> c;
	for(int i = 0 ; i < c; ++i)
	{
		int a, b, c;
		cin >> a >> b >> c;
		G[a].push_back({b,c});
		G[b].push_back({a, c});
	}
	int resp = INF;
	//cout << resp << '\n';
	for(int i = 1; i <= s; ++i)
	{
		djikstra(i);
		int ans = -INF;
		for(int j = 1; j <= s; ++j) 
			if(dist[j]!=0)
			{ 
				ans = max(ans, dist[j]); 	
				//cout << ans << '\n';
			}	
		resp = min(resp, ans);
		//cout << resp <<' '<< ans << '\n';
	}
	cout << resp << '\n';
	return 0;
}
