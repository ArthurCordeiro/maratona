#include <bits/stdc++.h>
#define MAXN 100000
using namespace std;

int a[25][61], n;

int main()
{
	cin >> n;
	for(int i = 0 ; i < n ; ++i)
	{
		int x, b; cin >> x >> b;
		a[x][b]++;
	}
	int resp = -1;
	for(int i = 0; i < 25; ++i)for(int j = 0; j < 60; ++j)
		resp = max(resp, a[i][j]);
	cout << resp << '\n';
	return 0;
}
