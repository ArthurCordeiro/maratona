/*#include <bits/stdc++.h>
#define MAXN 5005
using namespace std;

int table[MAXN][105], n, a[] = {2, 5, 10, 20, 50, 100}, have[505];
int main()
{
	cin >> n;
	for(int k = 0; k < 6; ++k) cin >> have[a[k]];
	int x, y;
	for(int i = 0; i < 6; ++i) table[0][a[i]] = 1;
	for(int i = 2; i <=  n; ++i)
		for(int j = 0; j < 6; ++j)
			for(int k = 1; k <= have[a[j]] && (i - k*a[j] >= 0) ; ++k)
			{	
				int var = k*a[j];
				x =  table[ i - var ][a[j]];
				y = ((j > 0) ? table[i][a[j - 1]] : 0);
				table[i][a[j]] = x + y; 
			}
	cout << table[n][100] << '\n';
	return 0;
}	
*/
#include <bits/stdc++.h>

using namespace std;

int n, a[] = {2, 5, 10, 20, 50, 100}, have[6], dp[5005];
int main()
{
	cin >> n;
	for(int i = 0 ; i < 6; ++i) cin >> have[i];
	dp[0] = 1;
	for (int i = 5; i >= 0; --i)
		for (int j = n; j >= 0; --j)
			for (int k = 1; k <= have[i] && j-k*a[i] >= 0; ++k)
				dp[j] += dp[j-k*a[i]];
	cout << dp[n] << '\n';
	return 0;			
}
