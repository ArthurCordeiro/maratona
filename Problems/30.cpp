#include <bits/stdc++.h>
#define MOD 1000000007
using namespace std;
int t, m, n, dp[55][100005];
int main()
{	
	cin >> t >> m >> n;
	
	for(int i = m; i<=n; ++i) dp[0][i] = 1;
	
	for(int i = 1; i < t; ++i)
		for(int j = m; j <= n; ++j)
			if(j == m)
				dp[i][j] = dp[i-1][j + 1];
			else 
				if(j == n) 
					dp[i][j] = dp[i-1][j - 1];
				else
					dp[i][j] = ( dp[i-1][j-1] + dp[i-1][j + 1])%MOD;
	int ans = 0;
	for(int j = m; j <= n; ++j) ans = (ans + dp[t-1][j])%MOD; 
	cout << ans << '\n';
	return 0;
}
//"-------------------------------------------------------------------------"
/*
#include <bits/stdc++.h>

#define int long long
using namespace std;
int t, m , n, pd[55][100005];
long long MOD = 1000000007;
int solve(int pos, int exerc)
{
	
	if(pos == 1) return 1;
	if(pd[pos][exerc] != -1 ) return pd[pos][exerc];
	int ans = 0;
	if(exerc > m) ans += (solve(pos - 1, exerc - 1)%MOD);
	if(exerc < n) ans += (solve(pos - 1, exerc + 1)%MOD);
	ans = ans%MOD;
	return pd[pos][exerc] = ans;
}
main()
{
	cin >> t >> m >> n;
	memset(pd, -1, sizeof(pd));
	int ans = 0;
	for(int i = m ; i <= n; ++i) ans += (solve(t, i)%MOD), ans %= MOD;
	ans %= MOD;
	cout << ans << '\n';
	return 0;
}*/
