/*#include <bits/stdc++.h>

using namespace std;
int n, m, m1[55][55], m2[55][55];
int main()
{
	cin >> n >> m;
	for(int i = 0 ; i < n ; ++i) 
		for(int j = 0 ; j < m ; ++j)	
			cin >> m1[i][j];
	for(int i = 0 ; i < n ; ++i) 
		for(int j = 0 ; j < m ; ++j)
			cin >> m2[i][j];
	
	for(int i = 0; i < n - 1; i ++)
		for(int j = i; j < m - 1; ++j)
		{
			if(m1[i][j] >= m1[i][j + 1] && m1[i][j] >= m1[i+1][j])
			{
				swap(m1[i][j], m2[i][j]);
				if(m1[i][j] >= m1[i][j + 1] || m1[i][j] >= m1[i+1][j])
				{ 
					swap(m1[i][j], m2[i][j]);
					swap(m1[i][j+1],m2[i][j+1]);
					swap(m1[i+1][j], m2[i+1][j]);
					if(m1[i][j] >= m1[i][j + 1] || m1[i][j] >= m1[i+1][j]){cout << "Impossible\n";}//return 0;}
				}
			}
			if(m1[i][j] >= m1[i][j + 1])
			{
				swap(m1[i][j], m2[i][j]);
				if(m1[i][j] >= m1[i][j + 1] || m1[i][j] >= m1[i+1][j])
				{ 
					swap(m1[i][j], m2[i][j]);
					swap(m1[i][j+1],m2[i][j+1]);
					if(m1[i][j] >= m1[i][j + 1] || m1[i][j] >= m1[i+1][j]){cout << "Impossible\n";}//return 0;}
					
				}
			}
			if(m1[i][j] >= m1[i+1][j])
			{ 
				swap(m1[i][j], m2[i][j]);
				if(m1[i][j] >= m1[i][j + 1] || m1[i][j] >= m1[i+1][j])
				{
					swap(m1[i][j], m2[i][j]);			
			 		swap(m1[i+1][j], m2[i+1][j]);
			 		if(m1[i][j] >= m1[i][j + 1] || m1[i][j] >= m1[i+1][j]){cout << "Impossible\n";}//return 0;}
				}
			}
		}
	for(int i = 0; i < n -1; ++i)
		for(int j = i; j < m - 1; ++j)
		{
			if(m2[i][j] >= m2[i][j + 1] || m2[i][j] >= m2[i+1][j])
			{
				swap(m1[i][j], m2[i][j]);
				if(m2[i][j] >= m2[i][j + 1] || m2[i][j] >= m2[i+1][j])
				{ cout << "Impossible\n";}// return 0;}
			}
		}
	for(int i = 0; i < n; ++i)
	{	
		for(int j = 0; j < m; ++j)
			cout << m1[i][j] << ' ';
		cout << '\n';
	}
	cout << "Possible";
	return 0;
}*/

#include <bits/stdc++.h>
#define FOR(i, n) for(int i = 0 ; i < n; ++i)
using namespace std;
int n, m, m1[55][55], m2[55][55];
int main()
{
	cin >> n >> m;
	FOR(i, n)
		FOR(j, m) cin >> m1[i][j];
	FOR(i, n)
		FOR(j, m) cin >> m2[i][j];
	
	FOR(i, n)
	FOR(j, m) 
	{
		int a = min(m1[i][j], m2[i][j]), b = max(m1[i][j], m2[i][j]);
		m1[i][j] = a, m2[i][j] = b;	
	}
	for(int i=0;i<n;i++)
		for(int j=0;j<m-1;j++)
			if(m1[i][j]>=m1[i][j+1] || m2[i][j]>=m2[i][j+1]){cout << "Impossible"; return 0;}


	for(int i=0;i<m;i++)
		for(int j=0;j<n-1;j++)
			if(m1[j][i]>=m1[j+1][i] || m2[j][i]>=m2[j+1][i]){cout << "Impossible"; return 0;}
	
	cout << "Possible";
	return 0;	
}	
	
