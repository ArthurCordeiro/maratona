/*
#include <bits/stdc++.h>

using namespace std;
vector<int> v1, v2 ;
int n, a[100];
bool find(int x, int n )
{
    int ini = 0, fim = n - 1, mid;
    while(ini <= fim)
    {
        mid = (ini + fim)/2;   
        if(v1[mid]==x) return true;
        else if(v1[mid] > x ) fim = mid - 1;
            else ini = mid + 1;
    
    }
    return false;
}
int main()
{
    scanf("%d", &n);
    for(int i = 0 ; i < n ; ++i) cin >> a[i];
    for(int i = 0; i < n ; ++i)
        for(int j = 0 ; j < n; ++j)
            for(int k = 0 ; k< n; ++k)
                v1.push_back(a[i] +a[j] +a[k]),v2.push_back(a[k]*(a[i]+a[j])) ;
        

    sort(v1.begin(), v1.end());
    sort(v2.begin(), v2.end());
    int t  = v2.size(), resp = 0;    
    for(int i = 0; i < t; ++i)
    {
        int low = lower_bound(v1.begin(), v1.end(),v2[i]) - v1.begin();
        int up = upper_bound(v1.begin(), v1.end(),v2[i]) - v1.begin();
        resp += up - low;
    } 
     printf("%d\n", resp);    
    return 0;    
 }

*/

#include <iostream>
#include <vector>
#include <algorithm>
#include <sstream>
#include <cstring>
#include <cmath>
 
using namespace std;
 
#define MAX_N 101
 
int n,x[MAX_N],lo,hi;
long long res=0LL;
vector<int>s1,s2;
 
int main()
{
    scanf("%d",&n);
    for (int i=0;i<n;i++)
    {
        scanf("%d",&x[i]);
    }
    for (int i=0;i<n;i++)
        for (int j=0;j<n;j++)
            for (int k=0;k<n;k++)
            {
                s1.push_back(x[i]*x[j]+x[k]);
                if (x[k]==0) continue;
                s2.push_back((x[i]+x[j])*x[k]);
            }
    sort(s1.begin(),s1.end());
    sort(s2.begin(),s2.end());
    for (int i=0;i<s1.size();i++)
    {
        lo=lower_bound(s2.begin(),s2.end(),s1[i])-s2.begin();
        hi=upper_bound(s2.begin(),s2.end(),s1[i])-s2.begin();
        res+=(hi-lo);
    }
    printf("%lld\n",res);
    return 0;
}
