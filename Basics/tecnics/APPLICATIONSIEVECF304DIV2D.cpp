#include <bits/stdc++.h>
#define MAXN 5000005

using namespace std;
// o problema e que o crivo nao conta um primo repitido no numero por exemplo o 
//algoritmo so identifica se o numero 4 tem 2 na fatoracao e nao 2*2

int t;
int is_prime[MAXN];
void sieve_erastothenes(int n)
{
	for(int i = 0; i <= MAXN; ++i) is_prime[i] = 0;
	is_prime[0] = is_prime[1] = 1;
	for (int i = 2; i <= n; i++) 
	{
    		if (is_prime[i] == 0 && (long long)i * i <= n) 
				{
        		for (int j = i * i; j <= n; j += i)
           	 		is_prime[j]++;
    		}
	}	
}

int main()
{
	cin >> t;
	sieve_erastothenes(MAXN);
	int a , b;
	while(t--)
	{
		int resp = 0;
		cin >> a >> b;
		for(int i = b + 1;i <= a; ++i)
			if(is_prime[i] == 0) resp++;
			else resp += is_prime[i];
		cout << resp << '\n'; 
	}
	return 0;
	
}
