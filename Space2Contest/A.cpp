#include <bits/stdc++.h>
#define bug(x) cout << #x << ' ' << x << '\n'
#define line cout << "----------------------------------------------------\n"
using namespace std;
typedef unsigned long long ull;

ull nzd(ull a, ull b)
{
	if(b==0) return a;
	a%=b;
		return nzd(b,a);
}
ull n,m;

int main()
{

	int n; cin >> n;
	while(n--)
	{
		ull x, x0, k = 0, it = 0; cin >> x;
		x0 = x;
		while(k <= 1)
		{
			ull sum =0;
			x = x0 + it;
			
			while(x > 0)
			{	
				sum += x % 10;
				x /= 10; 
			}		
			
			
			//bug(sum);
			//bug(it);
			//bug(nzd(x0, sum));
			//line;
			
			
			k = nzd(x0 + it, sum);
			it++;
		}
		cout << x0 + it - 1<< '\n';
	}	

	return 0;
}
