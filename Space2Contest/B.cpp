#include <bits/stdc++.h>

using namespace std;

int main()
{
	int t; cin >> t;
	
	while(t--)
	{
		int n; long w; cin >> n >> w;
		vector<long> vec;
		long x;
		for(int i = 0 ; i < n; ++i)
		{	cin >> x; vec.push_back(x);}
		sort(vec.begin(), vec.end());
		
		int indice = 0, last = n -1, resp = 0;
	
		while(indice != last)
		{
			int sum = vec[last];
			while(sum < w && indice < last)
			{
				sum += vec[indice];
				if(sum < w) indice++;
			}
			last--;
			resp++;
		}
		cout << resp << '\n';
		vec.clear();
	}
	return 0;
}
