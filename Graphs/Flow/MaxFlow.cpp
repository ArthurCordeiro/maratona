    #include <bits/stdc++.h>
    #define bug(x) cout << #x << ' ' << x << '\n'
     
    using namespace std;
     
    typedef pair<int, int> ii;
    const int OO = 0x3f3f3f3f;
    const int MAX = 1e4;
     
    struct edge
    {
        int v, f, c;
        edge(){}
        edge(int _v, int _f, int _c)
        {
            v = _v, f = _f, c = _c;
        }
    };
     
    vector<edge> edges;
    vector<int> G[MAX];
    int tempo = 1, cor[MAX], pai[MAX];
     
    void add_edge(int u, int v, int cp, int rc)
    {
        edges.push_back(edge(v, 0, cp));
        G[u].push_back(edges.size()-1);
        edges.push_back(edge(u, 0, rc));
        G[v].push_back(edges.size()-1);
    }
     
    int dfs(int s, int t, int f)
    {
        if(s == t) return f;
        cor[s] = tempo;
        for(int e : G[s])
            if(cor[edges[e].v] < tempo and edges[e].c-edges[e].f > 0)
                if(int a = dfs(edges[e].v, t, min(f, edges[e].c-edges[e].f)))
                {
                    edges[e].f += a;
                    edges[e^1].f -= a;
                    return a;
                }
        return 0;
    }
     
    int MaxFlow(int s, int t)
    {
        int mf = 0;
        while(int a = dfs(s, t, OO))
            mf += a, tempo++;
        return mf;
    }
    int meninos[105], meninas[105];
    int main()
    {
        int a, b;
    	cin >> a;
    	for(int i = 1 ; i <= a; ++i)
    	{
    		cin >> meninos[i];
    		add_edge(0,i, 1, 0);
    	    
    	}
    	cin >> b;
    	
    	for(int i = 1 ; i <= b; ++i)
    	{
    		cin >> meninas[i];
    		add_edge(a+i, a + b +1, 1, 0);
    	}
    	for(int i = 1 ; i <= a; ++i)
    		for( int j = 1 ; j <= b; ++j)
    			if(abs(meninos[i] - meninas[j]) <= 1)add_edge(i, j + a, 1, 0); 	
    	     
    	        
    	cout << MaxFlow(0, a + b +1) << '\n';
    	return 0;	
    }
