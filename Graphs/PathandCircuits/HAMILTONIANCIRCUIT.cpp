//Complexidade O(2^n*n²)
//If the degree of each node is at least n/2, the graph contains a Hamiltonian path.
//If the sum of degrees of each non-adjacent pair of nodes is at least n, the graph contains a Hamiltonian path.

#include <bits/stdc++.h>

using namespace std;
int G[21][21]; 
int table[21][(1 << 21)];//tabela de pd


int N;// numero de vertices do grafo

//funcao de Usando programacao dinâmica
int tsp(int pos, int bitmask)
{
    if(bitmask == (1<<N) - 1) return G[pos][0];//caso base da pd se todos
    											//os vertices foram visitados
   
    if(table[pos][bitmask] != -1) return table[pos][bitmask];//verifico ja
    //calculei o estado que eu quero, ou seja, o prefixo de uma sequencia
    
    int dist = INT_MAX; //distancia inicializada com o um valor qualquer
    
    
    
    // nesse loop ha, a construcao de todas as possibilidades  de sequencia
    for(int i = 0; i < N; ++i)
    {
        if(bitmask&(1<<i)) continue;//verifico se o bit esta on conjunto
        // se estiver pulo para a proxima iteracao
        
        
        dist = min(dist, G[pos][i] + tsp(i, bitmask|(1<<i)));
        //vai atualizar a dist, ja que o vertice atual nao esta na bitmask
   	}
    
    // retorno e salvo o valor na tabela de pd
    return table[pos][bitmask] = dist;
}

int main()
{
	memset(table, -1, sizeof(table));
	scanf("%d", &N);
	for(int i = 0; i < N; ++i)//leitura dos valores da matriz de entrada
		for(int j = 0; j < N; ++j)
			scanf("%d", &G[i][j]);
			
	printf("Valor encontrado no passeio\n");
	printf("%d\nValeu Falow!\n", tsp(0, 1));		
		
	return 0;

}
