#include <bits/stdc++.h>

using namespace std;
vector<int> G1, G2;
int grau_G1[1000], grau_G2[1000];

vector<pair<int, int>> read_edges() {
  vector<pair<int, int>> edges;
  string s, edge, aux;
  getline(cin, s);
  stringstream ss(s);
  while(getline(ss, edge, ')')) {
    while(!edge.empty() and !isdigit(edge[0]))
      edge = edge.substr(1);
    string U, V;    
    stringstream sss(edge);
    getline(sss, U, ',');
    getline(sss, V, ',');
    if(U.empty() or V.empty()) continue;
    int u = stoi(U);
    int v = stoi(V);
      edges.push_back({min(u, v), max(u, v)});
   }
   sort(edges.begin(), edges.end());
   return edges;
}

int main()
{
	vector<pair<int, int>> G1_check, G2_check;
	string however1, however2;

	getline(cin, however1);		
	vector<pair<int, int>> G1 = read_edges();
	for(auto u :  G1){grau_G1[u.first]++, grau_G1[u.second]++;}
	
  for(int i = 0; i < 19; ++i){G1_check.push_back({grau_G1[i], i});}
  sort(G1_check.begin(), G1_check.end());
  sort(grau_G1, grau_G1 + 19);
	
	getline(cin, however2);		
	vector<pair<int,int>> G2 = read_edges();
	for(auto u : G2){ grau_G2[u.first]++, grau_G2[u.second]++;}
  
  for(int i = 0; i < 19; ++i){G2_check.push_back({grau_G2[i], i});}
  sort(G2_check.begin(), G2_check.end());
  sort(grau_G2, grau_G2 + 19);
  
  int resp = 0;
	bool flag = true;
	for(int i = 0 ; i < 19; ++i){ if(grau_G1[i] != grau_G2[i]){flag = false;} resp+= abs(grau_G1[i] - grau_G2[i]);  }
	
	cout << (flag ? "Yes" : "No") << '\n';
	if(flag)
    for(int i = 0 ; i < 19; ++i)
    { 
      pair<int, int> u, v;
      u = G1_check[i], v = G2_check[i];
      cout << u.second << "-->" << v.second << '\n';
    }
  else
    cout << resp << '\n';
  return 0;
}
