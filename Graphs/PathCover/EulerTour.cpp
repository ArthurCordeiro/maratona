/* Euler Tour --> Make a dfs traversal starting at the root and build a 
list euler*/

/*list euler --> stores the order of the vertices that we visit (vertex is
added to the list when we first visit it); */

#include <bits/stdc++.h>
#define MAXN 100005
using namespace std;
bool visited[MAXN];
vector<int> height, euler, first;
vector<vector<int>> adj;


void tour(vector<vector<int>> &adj, int node, int h = 0) 
{
    visited[node] = true;
    height[node] = h;
    first[node] = euler.size();
    euler.push_back(node);
    for (auto to : adj[node]) 
    	if (!visited[to]) 
        {
            dfs(adj, to, h + 1);
            euler.push_back(node);
        }
    
}

int main()
{
	int root; cin >> root;
	height.resize(n);
    first.resize(n);
    euler.reserve(n * 2);
	tour(adj, root, 0);

	return 0;
}
