/* This code make a biparte check with this code we can solve the MINMUM VERTEX COVER on bipartite graphs. Suppose the graph G is given. Subset A of its vertices is called a vertex cover of this graph, if for each edge uv there is at least one endpoint of it in this set, i.e. or (or both). Vertex Cover Problem is a known NP Complete problem, i.e., there is no polynomial time solution for this unless P = NP. Unless if the given graph is a bipartite graph or a tree*/

#include <bits/stdc++.h>
#define MAXN 100005
using namespace std;
int n, m;
vector<int> G[MAXN];
bool marked[MAXN], colored[MAXN];
void dfs(int u)
{
	marked[u] = true;
	for(int v : G[u])
	{
		if(!marked[v])
		{	
			if(!colored[v] && !colored[u])
				colored[v] = true;
			dfs(v);	
		}
	}
}

int main()
{
	cin >> n >> m;
	for(int i = 0 ; i < m ; ++i)
	{
		int a, b; cin >> a >> b;
		G[a].push_back(b); G[b].push_back(a);
	}
	vector<int> resp[2];
	for()
	return 0;
}
