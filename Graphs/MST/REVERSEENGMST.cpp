#include <bits/stdc++.h>
#define MAXN 100005
using namespace std;
int n, m, pai[MAXN], size[MAXN];
vector<int> G[MAXN];
vector<pair<int, pair<int , int>>> edges;
int find(int x)
{
    while(x != pai[x]) x = pai[x];
    return x;
}
bool same(int a, int b)
{
    return (find(a) == find(b));
}
void unite(int a, int b)
{
    a = find(a);
    b = find(b);

    if(size[a] < size[b]) swap(a, b);
    size[a] += size[b];g
    pai[b] = a;
}


int main()
{
    for(int i = 1 ; i <= MAXN; ++i) pai[i] = i, size[i] = 1;
    cin >> n >> m;
    for(int i = 0 ; i < m ; ++i)
    {
        int a, b , c; cin >> a >> b >> c;
        edges.push_back({c,{a, b}});
    }
    int resp = 0;sort(edges.begin(), edges.end());
    for(auto i : edges)
    {
        int a = i.second.first, b = i.second.second;
        if(!same(a, b))
        {
            resp += i.first, G[a].push_back(b), G[b].push_back(a);
            unite(a, b);
        }
    }
    cout << resp;
    return 0;
}
