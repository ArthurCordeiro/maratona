/*
#include <bits/stdc++.h>
#define MAXN 100005
using namespace std;
int dist[MAXN], n, m, pai[MAXN];
bool visited[MAXN];
vector<pair<int , int>> G[MAXN];
void dijkstra(int x)
{
	priority_queue<pair<int,int>> q;
	for(int i = 1; i <= n; ++i) dist[i] = MAXN, pai[i] = i;
	dist[x] = 0;
	q.push({0, x});
	
	while(!q.empty())
	{
		int v = q.top().second; q.pop();
		if(visited[v]) continue;
		visited[v] = true;
		for(auto u : G[v])
		{
			int atual = u.first, w = u.second;  
			if(dist[v] + w < dist[atual])
			{
				pai[atual] = v;
				//cout << "entrei\n";
				dist[atual] = dist[v] + w;
				q.push({-dist[atual], atual});
			}
		}
	}
}
int main()
{
	cin >> n >> m;
	for(int i = 0; i < m; ++i)
	{	
		int a, b, c; cin >> a >> b >> c;
		G[a].push_back({b, c});G[b].push_back({a, c});
	}
	dijkstra(1);
	int x = n;
	vector<int> resp;
	while(pai[x] != x)
	{
		 
		resp.push_back(x);
		x = pai[x];
		if(x == pai[x] && x == 1) resp.push_back(1); 	
	}
	int k = resp.size() - 1;	 
	if(dist[n] == MAXN)
		cout << -1;
	else 
		for(int i = k; i >= 0; --i) cout << resp[i] << ' ';
	
	return 0;
}
*/
#include<bits/stdc++.h>
#define N 100001
using namespace std;
typedef long long int ll;
ll dist[N];
int pt[N];
vector<int>adj[N],cost[N];
void dijkastra(int n)
{
	ll i,u,a,v;
	for(i=0;i<=n;i++)
	pt[i]=dist[i]=-1;
	priority_queue<pair<int,int> >pq;
	pt[1]=1;
	pq.push(make_pair(0,1));
	dist[1]=0;
	while(!pq.empty())
	{
		u=pq.top().second;
		a=adj[u].size();
		pq.pop();
		for(i=0;i<a;i++)
		{
			v=adj[u][i];
			if(dist[u]+cost[u][i]<dist[v]||dist[v]==-1)
			{
				dist[v]=dist[u]+cost[u][i];
				pq.push(make_pair(-dist[v],v));
				pt[v]=u;
			}
		}
	}
}
vector<int>p;
void path(int end)
{
	ll t;
	if(dist[end]==-1)
	return;
	p.push_back(end);
	t=end;
	while(t!=1)
	{
		if(pt[t]==-1)
		{
			p.clear();
			return;
		}
		t=pt[t];
		p.push_back(t);
	}
}
int main()
{
	ll i,n,m;
	cin>>n>>m;
	ll u,v,w;
	for(i=0;i<m;i++)
	{
		cin>>u>>v>>w;
		adj[u].push_back(v);
		adj[v].push_back(u);
		cost[u].push_back(w);
		cost[v].push_back(w);
	}
	dijkastra(n);
	path(n);
	if(p.size()==0)
	cout<<-1;
	else
	{
		for(i=p.size()-1;i>=0;i--)
			cout<<p[i]<<" ";
		cout<<endl;
	}
	return 0;
}














