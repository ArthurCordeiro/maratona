// Given a tree T of N nodes, calculate longest path between any two nodes(also known as diameter of tree). 

#include <bits/stdc++.h>
using namespace std;

int n, m;
vector<int> G[10000];
int dp[100000], diam;

void dfs(int v, int p)
{
	if(G[v].size() == 1) dp[v] = 0;
	vector<int> aux;
	for(int &u : G[v])
		if(u != p)
		{
			dfs(u, v);
			aux.push_back(dp[u]);
			dp[v] = max(dp[u] + 1, dp[v]);
			if(aux.size() > 2)
			{
				sort(aux.rbegin(), aux.rend());
				aux.pop_back();
			}
		}
	sort(aux.rbegin(), aux.rend());
	int ans = 0;
	if(!aux.empty()) ans += aux[0] + 1;
	if(aux.size() > 1) ans += aux[1] + 1;
	diam = max(diam, ans);
}

int main()
{
	cin >> n >> m;
	while(m--)
	{
		int u, v;
		cin >> u >> v; u--; v--;
		G[u].push_back(v);
		G[v].push_back(u);
	}
	dfs(0, -1);
	cout << diam << '\n';

	return 0;
}

