/* the idea is to construct a tree traversal array that contains three values for each node: the iden
tifier of the node, the size of the subtree, and the pathsum.*/
 

#include <bits/stdc++.h>
#define MAXN 100005

using namespace std;
vector<int> G[MAXN];
bool visited[MAXN];
int subtreesize[MAXN], pathsum[MAXN], node[MAXN];
int n, m;

void dfs(int u)
{
	visited[u] = true;
	for(int v : G[u])
		if(!visited[v])
		{
			subtreesize[u]++;
			dfs(v);
			pathsum[u] += pathsum[v];
		}
}


int main()
{
	cin >> n >> m;
	for(int i = 1; i <= n ; ++i) cin >> node[i];
	
	int a, b;
	for(int i = 0; i < m; ++i)
	{
		cin >> a >> b;
		G[a].push_back(b);
	}
	for(int i = 1; i <= n; ++i) subtreesize[i] = 1, pathsum[i] = node[i];
	
	cout << "Nodes \t";
	for(int i = 1; i <= n ; ++i)
		cout << i << ' ';
	cout << '\n';
	
	cout << "subtreesize \t";
	for(int i = 1; i <= n ; ++i)
		cout << subtreesize[i] << ' ';
	cout << '\n';
	
	cout << "pathsum \t";
	for(int i = 1; i <= n ; ++i)
		cout << pathsum[i] << ' ';
	cout << '\n';
	return 0;
}
