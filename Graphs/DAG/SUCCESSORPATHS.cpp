#include <bits/stdc++.h>
#define MAXN 100005
using namespace std;

/* In those graphs, the outdegree of each node is 1, i.e., exactly one edge starts at each node. A successor graph consists of one or more components, each of which contains one cycle and some paths that lead to it.*/


int binaryNum[32], graph[MAXN][2];

/*void dectoBinary(int n) 
{  
  
    int i = 0; 
    while (n > 0) 
    { 
        binaryNum[i] = n % 2; 
        n = n / 2; 
        i++; 
    } 
	//for(int j = i - 1; j >= 0; --j)
	//	cout << binaryNum[j];
	return i - 1;
}*/ 
int succ(int x, int k)
{
	if(k == 1) return table[x][k] = graph[x][k];
	return table[x][k] = succ(succ(x, k/2), k/2);		
}

int main()
{
	int n, k; cin >> n >> k;
	//dectoBinary(k);
  /*int ans = n;
  for(int j = i - 1; j >= 0; --j)
  	if(binaryNum[j]) ans = succ(ans, j);
	*/  
  cout << succ(n, k) << '\n';
  return 0;
}
