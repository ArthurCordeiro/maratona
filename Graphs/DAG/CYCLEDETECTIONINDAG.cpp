#include <bits/stdc++.h>
#define MAXN 100005
#define gray 1
#define black 2
#define white 0

using namespace std;
vector<int> G[MAXN];
int n, m, color[MAXN];
bool visited[MAXN],hasCycle; 

void dfs_cycle(int v)
{
    visited[v] = true;
    color[v] = gray;
    
    for(int u : G[v])
        if(visited[u] && color[u] == gray && color[v] == gray) hasCycle= true;
        else if(!visited[u])dfs_cycle(u);
    
    color[v] = black;
}

int main()
{
    for(int i = 1; i <= n; ++i) color[i] = white;
    hasCycle = false;   
    cin >> n >> m; 
    int x, y;
    for(int i = 0; i < m;++i)
        cin >> x >> y, G[x].push_back(y); 
    for(int i = 1; i <= n; ++i)
        if(!visited[i]) dfs_cycle(i);
    
    if(hasCycle)
        cout << "Cycle Exist\n";
    else cout << "Cycle donsnt Exist\n";    
    return 0;
}

