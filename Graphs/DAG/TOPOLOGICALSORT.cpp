#include <bits/stdc++.h>
#define MAXN 100005
using namespace std;
int n;
vector<int> G[MAXN];
bool visited[MAXN];

vector<int> ans;

void dfs_sort(int v)
{
    visited[v] = true;
    for(int u : G[v])
        if(!visited[u])
            dfs(u);
    ans.push_back(v);
}

void topological_sort()
{
    memset(visited, false, sizeof(visited));
    for(int i = 0 ; i < n; ++i)
        if(!visited[i])
            dfs_sort(i);

    reverse(ans.begin(), ans.end());
} 
int main()
{



    return 0;
}
